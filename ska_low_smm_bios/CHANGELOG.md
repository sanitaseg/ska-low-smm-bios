# Changelog

## v1.6.0 (2023-10-16)
### Features
- increase PSU monitor points and speed-up get_health_status

## v1.5.0 (2023-10-11)
### Bug Fixes
- phantom PRESENT into SLOT1 at startup when not populated (introduced in v1.3.0)

## v1.4.0 (2023-10-05)
### Bug Fixes
- I2C buses frequency lowered from 400kHz to 100kHZ to prevent marginality

## v1.3.0 (2023-09-27)
### Bug Fixes
- SFP configuration failed
- constrained PPS to prevent propagation delay fluctuation between BIOS versions

## v1.2.0 (2023-09-08)
### Bug Fixes
- occasional failure on TPM network configuration on SLOTs power on (unreachable TPM)

## v1.1.0 (2023-06-13)
### Features
- CPU status heartbeat LED
- BOOT_SEL register

## v1.0.0 (2023-05-11)

First release

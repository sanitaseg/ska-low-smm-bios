BIOS_REV_list = [
    {
        'name': 'smm_pre_v1.2.4',
        'HW_REV_ge': None,
        'HW_REV_l': 0x010204,
        'bios': [
            {
                'rev': '0.9.9',
                'cpld': {
                    'id': '0xbe7a1015_0x202106251342',
                    'file': 'v0.9.9/ska_management_impl1.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00005-gda75be7d',
                    'file': 'v1.0.0/u-boot_2018.03-00005-gda75be7d.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00003-gffba12ad9',
                    'file': [
                        'v1.0.0/zImage_4.14.98-0002-00003-gffba12ad9.zImage',
                        'v1.0.0/ska-management_4.14.98-0002-00003-gffba12ad9.dtb',
                    ]},
            },
            {
                'rev': '1.0.0',
                'cpld': {
                    'id': '0xbe7a1014_0x202106150954',
                    'file': 'v1.0.0/ska_management_202106150954.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00005-gda75be7d',
                    'file': 'v1.0.0/u-boot_2018.03-00005-gda75be7d.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00003-gffba12ad9',
                    'file': [
                        'v1.0.0/zImage_4.14.98-0002-00003-gffba12ad9.zImage',
                        'v1.0.0/ska-management_4.14.98-0002-00003-gffba12ad9.dtb',
                    ]},
            },
            {
                'rev': '1.1.0',
                'cpld': {
                    'id': '0x3001014_0x202306131220',
                    'file': 'v1.1.0/ska_management_202306131220.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00004-g895cd15fa',
                    'file': [
                        'v1.1.0/zImage_4.14.98-0002-00004-g895cd15fa.zImage',
                        'v1.1.0/ska-management_4.14.98-0002-00004-g895cd15fa.dtb',
                    ]},
            },
            {
                'rev': '1.2.0',
                'cpld': {
                    'id': '0x8001014_0x202309081203',
                    'file': 'v1.2.0/ska_management_202309081203.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00004-g895cd15fa',
                    'file': [
                        'v1.1.0/zImage_4.14.98-0002-00004-g895cd15fa.zImage',
                        'v1.1.0/ska-management_4.14.98-0002-00004-g895cd15fa.dtb',
                    ]},
            },
            {
                'rev': '1.3.0',
                'cpld': {
                    'id': '0x9001014_0x202309271150',
                    'file': 'v1.3.0/ska_management_202309271150.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00004-g895cd15fa',
                    'file': [
                        'v1.1.0/zImage_4.14.98-0002-00004-g895cd15fa.zImage',
                        'v1.1.0/ska-management_4.14.98-0002-00004-g895cd15fa.dtb',
                    ]},
            },
            {
                'rev': '1.4.0',
                'cpld': {
                    'id': '0xa001014_0x202310051820',
                    'file': 'v1.4.0/ska_management_202310051820.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00004-g895cd15fa',
                    'file': [
                        'v1.1.0/zImage_4.14.98-0002-00004-g895cd15fa.zImage',
                        'v1.1.0/ska-management_4.14.98-0002-00004-g895cd15fa.dtb',
                    ]},
            },
            {
                'rev': '1.5.0',
                'cpld': {
                    'id': '0xb001014_0x202310111459',
                    'file': 'v1.5.0/ska_management_02310111459.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00004-g895cd15fa',
                    'file': [
                        'v1.1.0/zImage_4.14.98-0002-00004-g895cd15fa.zImage',
                        'v1.1.0/ska-management_4.14.98-0002-00004-g895cd15fa.dtb',
                    ]},
            },
            {
                'rev': '1.6.0',
                'cpld': {
                    'id': '0xb001014_0x202310111459',
                    'file': 'v1.5.0/ska_management_02310111459.bit'},
                'mcu': {
                    'id': '0xdb000103_0x2023101600160650',
                    'file': 'v1.6.0/Management_0xdb000103_20231016_4.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00006-g95ff03823',
                    'file': [
                        'v1.6.0/zImage_4.14.98-0002-00006-g95ff03823.zImage',
                        'v1.6.0/ska-management_4.14.98-0002-00006-g95ff03823.dtb',
                    ]},
            },
        ],
    },
    {
    'name': 'smm_v1.2.4',
        'HW_REV_ge': 0x010204,
        'HW_REV_l': None,
        'bios': [
            {
                'rev': '0.9.9',
                'cpld': {
                    'id': '0xbe7a1015_0x202106251342',
                    'file': 'v0.9.9/ska_management_impl1_XO3L.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00005-gda75be7d',
                    'file': 'v1.0.0/u-boot_2018.03-00005-gda75be7d.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00003-gffba12ad9',
                    'file': [
                        'v1.0.0/zImage_4.14.98-0002-00003-gffba12ad9.zImage',
                        'v1.0.0/ska-management_4.14.98-0002-00003-gffba12ad9.dtb',
                    ]},
            },
            {
                'rev': '1.0.0',
                'cpld': {
                    'id': '0xbe7a1014_0x202106150954',
                    'file': 'v1.0.0/ska_management_XO3L_202106150954.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00005-gda75be7d',
                    'file': 'v1.0.0/u-boot_2018.03-00005-gda75be7d.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00003-gffba12ad9',
                    'file': [
                        'v1.0.0/zImage_4.14.98-0002-00003-gffba12ad9.zImage',
                        'v1.0.0/ska-management_4.14.98-0002-00003-gffba12ad9.dtb',
                    ]},
            },
            {
                'rev': '1.1.0',
                'cpld': {
                    'id': '0x3001014_0x202306131220',
                    'file': 'v1.1.0/ska_management_XO3L_202306131220.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00004-g895cd15fa',
                    'file': [
                        'v1.1.0/zImage_4.14.98-0002-00004-g895cd15fa.zImage',
                        'v1.1.0/ska-management_4.14.98-0002-00004-g895cd15fa.dtb',
                    ]},
            },
            {
                'rev': '1.2.0',
                'cpld': {
                    'id': '0x8001014_0x202309081203',
                    'file': 'v1.2.0/ska_management_XO3L_202309081203.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00004-g895cd15fa',
                    'file': [
                        'v1.1.0/zImage_4.14.98-0002-00004-g895cd15fa.zImage',
                        'v1.1.0/ska-management_4.14.98-0002-00004-g895cd15fa.dtb',
                    ]},
            },
            {
                'rev': '1.3.0',
                'cpld': {
                    'id': '0x9001014_0x202309271150',
                    'file': 'v1.3.0/ska_management_XO3L_202309271150.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00004-g895cd15fa',
                    'file': [
                        'v1.1.0/zImage_4.14.98-0002-00004-g895cd15fa.zImage',
                        'v1.1.0/ska-management_4.14.98-0002-00004-g895cd15fa.dtb',
                    ]},
            },
            {
                'rev': '1.4.0',
                'cpld': {
                    'id': '0xa001014_0x202310051820',
                    'file': 'v1.4.0/ska_management_XO3L_202310051820.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00004-g895cd15fa',
                    'file': [
                        'v1.1.0/zImage_4.14.98-0002-00004-g895cd15fa.zImage',
                        'v1.1.0/ska-management_4.14.98-0002-00004-g895cd15fa.dtb',
                    ]},
            },
            {
                'rev': '1.5.0',
                'cpld': {
                    'id': '0xb001014_0x202310111459',
                    'file': 'v1.5.0/ska_management_XO3L_02310111459.bit'},
                'mcu': {
                    'id': '0xdb000102_0x2021040600125020',
                    'file': 'v1.0.0/Management-F0xdb000102.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00004-g895cd15fa',
                    'file': [
                        'v1.1.0/zImage_4.14.98-0002-00004-g895cd15fa.zImage',
                        'v1.1.0/ska-management_4.14.98-0002-00004-g895cd15fa.dtb',
                    ]},
            },
            {
                'rev': '1.6.0',
                'cpld': {
                    'id': '0xb001014_0x202310111459',
                    'file': 'v1.5.0/ska_management_XO3L_02310111459.bit'},
                'mcu': {
                    'id': '0xdb000103_0x2023101600160650',
                    'file': 'v1.6.0/Management_0xdb000103_20231016_4.bin'},
                'uboot': {
                    'id': '2018.03-00007-g4922a1d2',
                    'file': 'v1.1.0/u-boot_2018.03-00007-g4922a1d2.imx',
                },
                'krn': {
                    'id': '4.14.98-0002-00006-g95ff03823',
                    'file': [
                        'v1.6.0/zImage_4.14.98-0002-00006-g95ff03823.zImage',
                        'v1.6.0/ska-management_4.14.98-0002-00006-g95ff03823.dtb',
                    ]},
            },
        ],
    },
]

def get_bios(bios):
    bios_dict={}
    bios_dict['rev']='v'+bios['rev']
    for key,value in bios.items():
        if key != 'rev':
            bios_dict[key] = value['id']
    
    _first_key = True
    string=""
    for key,value in bios_dict.items():
        if key != 'rev':
            if not _first_key:
                string +='-'
            string += key + "_" + value
            _first_key = False
    string = "v%s (%s)" % (bios['rev'], string)
    return string,bios_dict

def bios_id_format(bios):
    string= ""
    _first_key = True
    for key,value in bios.items():
        if key != 'rev':
            if not _first_key:
                string +='-'
            string += key + "_" + value['id']
            _first_key = False
    return string

def check_hw_rev(_elem,hw_rev):
    res_ge = True
    res_l = True
    if _elem['HW_REV_ge'] is not None:
        res_ge = hw_rev >= _elem['HW_REV_ge']
    if _elem['HW_REV_l'] is not None:
        res_l = hw_rev < _elem['HW_REV_l']
    return res_ge and res_l

def bios_get(name=None,hw_rev=None):
    for _elem in BIOS_REV_list:
        if name is not None:
            if name == _elem['name']:
                return _elem
        if hw_rev is not None:
            if check_hw_rev(_elem, hw_rev):
                return _elem
                
def bios_get_dict(name=None,hw_rev=None):
    for _elem in BIOS_REV_list:
        if name is not None:
            if name == _elem['name']:
                _result = []
                for _bios in _elem['bios']:
                    _res_bios = [_bios['rev'], bios_id_format(_bios)]
                    _result.append(_res_bios)
                return _result
        if hw_rev is not None:
            if check_hw_rev(_elem, hw_rev):
                _result = []
                for _bios in _elem['bios']:
                    _res_bios = [_bios['rev'], bios_id_format(_bios)]
                    _result.append(_res_bios)
                return _result
            
    return None

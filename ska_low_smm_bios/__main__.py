__author__ = 'Cristian Albanese'
import os
import sys
import time
import tabulate
from pkg_resources import resource_filename
import logging
import parse
from functools import reduce


import ska_low_smm_bios
import cpld_mng_api
import subrack_mng_api

def check_update_files(path, bios):
    dirs = os.listdir(path)
    if dirs is not None:
        # print(dirs)
        dirs_ordered = sorted(dirs)
        # print(dirs_ordered)
        # folder = dirs_ordered[len(dirs_ordered)-1]
        folder = ""
        for i in range(0, len(dirs_ordered)):
            if dirs_ordered[i].find(bios) != -1:
                folder = dirs_ordered[i]
        if folder == "":
            print("Error: request bios folder not found")
            return "", ""
        # print folder
        fws = os.listdir(path+folder+"/")
        if len(fws) < 2:
            print("Error, incorrect number of file in path")
        else:
            # print(fws)
            fw_mcu = ""
            fw_cpld = ""
            for i in range(0, len(fws)):
                if fws[i].find("MCU") != -1 and fws[i].find(".bin") != -1:
                    fw_mcu = fws[i]
                if fws[i].find("XO3L") != -1 and fws[i].find(".bit") != -1:
                    fw_cpld = fws[i]
            if (fw_mcu!="" and fw_cpld!=""):
                print("FW MCU %s" % fw_mcu)
                print("FW CPLD %s" % fw_cpld)
                return fw_mcu, fw_cpld
            else:
                print("Error: unexpected file")
                return "", ""

def file_exist(file_path):
    if not os.path.exists(file_path):
        print("Error: cannot find file:")
        print(file_path)
        sys.exit(1)

def update_device(mng,device,fw,krn_dest_device = None):
    if device == 'cpld':
        return mng.update_cpld(fw[0])
    elif device == 'mcu':
        return mng.update_mcu(fw[0])
    elif device == 'uboot':
        return mng.flash_uboot(fw[0])
    elif device == 'krn':
        print(fw)
        zImage_path = [i for i in fw if "zImage" in i][0]
        dtb_path = [i for i in fw if "dtb" in i][0]
        return mng.update_kernel(zImage_path=zImage_path,dtb_path=dtb_path,dest_device=krn_dest_device)
    return None

ip2int = lambda ip: reduce(lambda a, b: (a << 8) + b, map(int, ip.split('.')), 0)

def int2ip(value):
    ip = str((value >> 24)&0xff)
    ip += "."
    ip += str((value >> 16)&0xff)
    ip += "."
    ip += str((value >> 8)&0xff)
    ip += "."
    ip += str((value >> 0)&0xff)
    return ip

if __name__ == "__main__":
    #logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s', datefmt='%m/%d/%Y %H:%M:%S',level=logging.DEBUG)
    #logger=logging.getLogger(os.path.basename(__file__)+"_main")

    # Use OptionParse to get command-line arguments
    import argparse
    from sys import argv, stdout

    parser = argparse.ArgumentParser(prog='ska_low_smm_bios',
                    description='SMM bios update tool and network configuration',
                    epilog='')
    parser.add_argument("--bios", help="Bios version to be loaded (e.g. v0.2.0 or 0.2.0)")
    parser.add_argument("--change-ip", help="Change CPU IP stored in eeprom, WARNING: next 15 ip addresses reserved (e.g. 10.0.10.32) ")
    parser.add_argument("--change-netmask", help="Change NETMASK stored in eeprom (e.g. 255.255.255.0) ")
    parser.add_argument("--change-gateway", help="Change GATEWAY stored in eeprom (e.g. 10.0.10.1) ")
    parser.add_argument("--force", action="store_true", help="skip confimations and final check")
    parser.add_argument("--emmc-format", action="store_true", help="format the eMMC non-volatile memory (no uSD)")
    parser.add_argument("--emmc-0-write", help="Provide TGZ file to write eMMC-0 partition (bios option required to write also kernel in required partition)")
    parser.add_argument("--emmc-1-write", help="Provide TGZ file to write eMMC-1 partition (bios option required to write also kernel in required partition)")
    parser.add_argument("--boot-sel-krn", type=int, help="Select partition for kernel (e.g. 0 or 1)")
    parser.add_argument("--boot-sel-fs", type=int, help="Select partition for filesystem (e.g. 0 or 1)")
    parser.add_argument("--show-changelog", action="store_true", help="Show bios CHANGELOG")
    parser.add_argument("--show-license", action="store_true", help="Show bios LICENSE")
    parser.add_argument("--show-bios", action="store_true", help="Show bios list")
    
    conf = parser.parse_args()
    if conf.show_changelog:
        print("=== CHANGELOG.md ===")
        f=open(resource_filename('ska_low_smm_bios','CHANGELOG.md'),'r')
        print(f.read())
        sys.exit(0)
    if conf.show_license:
        print("=== LICENSE ===")
        f=open(resource_filename('ska_low_smm_bios','LICENSE'),'r')
        print(f.read())
        sys.exit(0)
    if conf.show_bios:
        config=ska_low_smm_bios.bios.BIOS_REV_list
        print("Available bios:")
        for _board in config:
            print(" + "+_board['name'])
            for _bios in _board['bios']:
                print("     - v%s (%s-%s)"%(_bios['rev'],_bios['cpld']['id'],_bios['mcu']['id']))
            sys.exit(1)
    print("==============================================================")
    print("PLEASE READ THE AGREEMENT CAREFULLY.")
    print("BY USING THIS SOFTWARE, YOU ACCEPT THE TERMS OF THE AGREEMENT.")
    print("You can read license by '--show-license' option")
    print("==============================================================")
    print("")
    try:
        Mng = subrack_mng_api.management.Management()
        board_info=Mng.board_info
        table=[]
        for key in board_info:
            table.append([str(key), str(board_info[key])])
        print(tabulate.tabulate(table,headers=["BOARD INFO",""],tablefmt='pipe'))
        print("")
        hardware_rev=Mng.hw_rev
    except:
        print("ERROR - No BOARD found at ip address %s"%conf.ip)
        sys.exit(1)
        
    if conf.emmc_format:
        size = Mng.emmc_get_size()
        print(size)
        if size < 10:
            layout_file = "emmc_layout_8G_4part.layout"
        else:
            layout_file = "emmc_layout_16G_4part.layout"
        Mng.emmc_config(layout_file)

    if conf.boot_sel_krn is not None:
        if not conf.boot_sel_krn in[0, 1]:
            print("Error in boot-sel-krn, must be 0 or 1")
            sys.exit(1)
        Mng.set_krn_boot_partition("EMMC%d"%conf.boot_sel_krn)

    if conf.boot_sel_fs is not None:
        if not conf.boot_sel_fs in[0, 1]:
            print("Error in boot-sel-fs, must be 0 or 1")
            sys.exit(1)
        Mng.set_fs_boot_partition("EMMC%d"%conf.boot_sel_fs)

    if conf.change_ip is not None or conf.change_netmask is not None or conf.change_gateway is not None:
        new_ip=ip2int(board_info['CPLD_ip_address_eep'])-6
        new_netmask=ip2int(board_info['CPLD_netmask_eep'])
        new_gateway=ip2int(board_info['CPLD_gateway_eep'])
        if conf.change_netmask is not None:
            if conf.change_netmask not in ["255.255.255.0","255.255.0.0","255.0.0.0"]:
                print("WARNING - Netmask provided is not standard.")
                if not conf.force:
                    answ = input("Do you want continue (y/N)\n")
                    if answ.lower() != "y":
                        sys.exit(1)
            new_netmask = ip2int(conf.change_netmask)
        if conf.change_ip is not None:
            print("=============== WARNING !!! ===================")
            print("Error in netwrok configuration may leads to unreachable board.")
            print("")
            print("Below ip addresses MUST be reserved for board function:")
            new_ip=ip2int(conf.change_ip)
            if not new_ip & 0xff < 240:
                print("ERROR - Next 15 ip address reserved, requested ip should be less then x.x.x.240")
                sys.exit(1)
            ip_labels=["reserved"]*16
            ip_labels[0]="CPU"
            ip_labels[6]="CPLD"
            for i in range(8):
                ip_labels[7+i]="SLOT-%d TPM"%(i+1)
            table=[]
            for i in range(16):
                table.append([int2ip(new_ip+i),ip_labels[i]])
            print(tabulate.tabulate(table,headers=["RESERVED IPs",""],tablefmt='pipe')) 
            print("")

        
        if conf.change_gateway is not None:
            new_gateway=ip2int(conf.change_gateway)
            if new_ip <= new_gateway and new_gateway <= new_ip + 15:
                print("ERROR - Gateway in reserved range")
                sys.exit(1)

        
        table=[]
        table.append(["CPU  ip address",(board_info['CPU_ip_address']),int2ip(new_ip)])
        table.append(["CPLD ip address",board_info['CPLD_ip_address_eep'],int2ip(new_ip+6)])
        table.append(["netmask",board_info['CPLD_netmask_eep'],int2ip(new_netmask)])
        table.append(["gateway",board_info['CPLD_gateway_eep'],int2ip(new_gateway)])
        print(tabulate.tabulate(table,headers=["","ACTUAL","NEW"],tablefmt='pipe'))           
        if not conf.force:
            answ = input("Do you want continue (y/N)\n")
            if answ.lower() != "y":
                sys.exit(1)

        if conf.change_ip is not None:
            Mng.set_field('ip_address',int2ip(new_ip+6))

        if conf.change_netmask is not None:
            Mng.set_field('netmask',int2ip(new_netmask))

        if conf.change_gateway is not None:
            Mng.set_field('gateway',int2ip(new_gateway))

        sys.exit(0)
    
    _elem=ska_low_smm_bios.bios.bios_get(hw_rev=hardware_rev)
    # print(_elem)

    if _elem is None:
        print("ERROR - No bios match for HW_rev 0x%x"%hardware_rev)
        sys.exit(1)

    bios_match = False
    if conf.bios is not None:
        for _bios in _elem['bios']:
            if conf.bios.lstrip('v') == _bios['rev']:
                bios_match=True
                break
    if conf.bios is None or not bios_match:
        print("ERROR - No bios provided or bios not available")
        print("Please specify one of bios below compatible with HW_rev 0x%x"%hardware_rev)
        for _bios in _elem['bios']:
            print(" - v%s (%s-%s)"%(_bios['rev'],_bios['cpld']['id'],_bios['mcu']['id']))
        sys.exit(1)
    
    #r=parse.parse("v{}.{}.{} (CPLD_{}-MCU_{}-KRN_{})",board_info['bios'])
    act_bios,act_bios_dict=Mng.get_bios()
    # bios_id['cpld']=r[3]
    # bios_id['mcu']=r[4]
    # bios_id['krn']=r[5]
    #print(r)
    fw={}
    update={}
    for _device in ['cpld','mcu','uboot','krn']:
        fw[_device]=[]
        if type(_bios[_device]['file']) == list:
            _files=_bios[_device]['file']
        else:
            _files=[_bios[_device]['file']]
        for _file in _files:
            if os.path.isabs(_file):
                print("ERROR - %s FILE path must be relative"%_device)
                print(_file)
                sys.exit(1)
            else:
                fw[_device].append(resource_filename('ska_low_smm_bios',_file))
            # print(_device,_file)
            file_exist(fw[_device][-1])
        if act_bios_dict[_device] != _bios[_device]['id']:
            update[_device]=True
        else:
            update[_device]=False
    
    req_bios,req_bios_dict = ska_low_smm_bios.bios.get_bios(_bios)
    
    table=[]
    for key,value in act_bios_dict.items():
        if act_bios_dict[key] != req_bios_dict[key]:
            diff = "*"
        else:
            diff = ""
        table.append([key,act_bios_dict[key],req_bios_dict[key],diff])
    print(tabulate.tabulate(table,headers=["BIOS","ACTUAL","REQUESTED","diff"],tablefmt='pipe'))
    print("")

    if True not in update.values():
        print("INFO - Board already updated to the required version")
    else:
        print("=============== WARNING !!! ===================")
        print("SMM BIOS Update procedure ready to start.")
        print("This procedure will delete the board BIOS.")
        print("You execute this procedure at your risk.")
        print("Do NOT power off the board, while process start.")
        if not conf.force:
            answ = input("Do you want continue (y/N)\n")
            if answ != "y" and answ != "Y":
                sys.exit(1)

        for device,_update in update.items():
            if _update:
                if update_device(Mng,device,fw[device]) != 0:
                    print("Error while %s UPDATE, please verify and retry without poweroff board"%device)
                    sys.exit(1)

    fw = fw['krn']
    zImage_path = [i for i in fw if "zImage" in i][0]
    dtb_path = [i for i in fw if "dtb" in i][0]
    emmc_write = [conf.emmc_0_write, conf.emmc_1_write]  
    for idx,fs_tgz in enumerate(emmc_write):
        if fs_tgz is not None:
            print("Execiting emmc_%d_write write_kernel"%idx)
            if Mng.write_kernel(zImage_path=zImage_path,dtb_path=dtb_path,dest_device="EMMC%d"%idx) > 0:
                print("Error")
                sys.exit(1)
            print("Execiting emmc_%d_write flash_fs_image"%idx)
            if Mng.flash_fs_image(fs_tgz,"EMMC%d"%idx) > 0:
                print("Error")
                sys.exit(1)

    print("Please power off and power on the board, and verify bios manually")
    sys.exit(0)

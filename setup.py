import os
import sys
from pathlib import Path
from setuptools import setup, find_packages

def find_all_files(directory, endings=None, names=None):
    """
    Recursively find all files within directory
    """
    result = []
    for root, _, filenames in os.walk(directory):
        for filename in filenames:
            if endings is None and names is None:
                result.append(str(Path(root) / filename))
                print("Adding : ",str(Path(root) / filename))
            elif endings is not None:
                for ending in endings:
                    if filename.endswith(ending):
                        result.append(str(Path(root) / filename))
                        print("Adding by ending : ",str(Path(root) / filename))
                        break
            if names is not None:
                str_filename = os.path.basename(filename)
                if filename in names:
                    result.append(str(Path(root) / filename))
                    print("Adding by name: ",str(Path(root) / filename))
    return result

DATA_FILES = []
DATA_FILES += find_all_files(str(Path("ska_low_smm_bios")), endings=[".bit"])
DATA_FILES += find_all_files(str(Path("ska_low_smm_bios")), endings=[".bin"])
DATA_FILES += find_all_files(str(Path("ska_low_smm_bios")), endings=["zImage"])
DATA_FILES += find_all_files(str(Path("ska_low_smm_bios")), endings=[".dtb"])
DATA_FILES += find_all_files(str(Path("ska_low_smm_bios")), endings=[".imx"])
DATA_FILES += find_all_files(str(Path("ska_low_smm_bios")), names=["CHANGELOG.md"])
DATA_FILES += find_all_files(str(Path("ska_low_smm_bios")), names=["LICENSE"])
DATA_FILES = [os.path.relpath(file_name, "ska_low_smm_bios") for file_name in DATA_FILES]

setup(
    name='ska-low-smm-bios',
    version='1.6.0',
    packages=['ska_low_smm_bios'],
    package_data={'ska_low_smm_bios': DATA_FILES},
    zip_safe=False,
    url='https://gitlab.com/sanitaseg/ska-low-smm-bios.git',
    license='',
    author='Sanitas EG',
    author_email='info@sanitaseg.it',
    description='BIOS for SMM board',
    dependency_links=[],
    install_requires=["optparse-pretty","tabulate","console_progressbar"],
)
